import { Component } from '@angular/core';
import { AES256 } from '@ionic-native/aes-256/ngx';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  textoClaro: string = "";
  textCifrado: string = "";
  textoDescifrado: string = "";
  password: string = "";
  secureKey: string = "";
  secureIV: string = "";

  constructor(private aes256: AES256) {}

  generarKeyIV(){
    if (this.password == "")  return;

    this.aes256.generateSecureKey(this.password)
    .then(key => {
      this.secureKey = key;
    })
    .catch(error => {
      this.secureKey = "";
    });
    
    this.aes256.generateSecureIV(this.password)
    .then(iv => {
      this.secureIV = iv;
    })
    .catch(error => {
      this.secureIV = "";
    });
  }

  encriptar(){
    if ((this.textoClaro == "") || (this.secureKey == "") || (this.secureIV == "")) return;

    this.aes256.encrypt(this.secureKey, this.secureIV, this.textoClaro)
    .then(res => {
      this.textCifrado = res;
    })
    .catch(error => {
      this.textCifrado = "";
    });
  }

  desencriptar(){
    if ((this.textCifrado == "") || (this.secureKey == "") || (this.secureIV == "")) return;

    this.aes256.decrypt(this.secureKey, this.secureIV, this.textCifrado)
    .then(res => {
      this.textoDescifrado = res;
    })
    .catch(error => {
      this.textoDescifrado = "";
    });
  }

}
